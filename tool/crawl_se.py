from bs4 import BeautifulSoup
import json

def get_price(text):
    CHAR = "abcdefghijklmnopqrstuvwxyzáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ € P:"
    for char in CHAR:
        # print(char)
        if char in text:
            text = text.replace(char, '')
    return text

def clean_text(text):
    text = text[:-1]
    text = text[1:]
    return text
def get_value(string):
    ls_temp = string.split('"')
    ls_temp = ls_temp[-2:-1]
    if ls_temp:
        if ls_temp[0]=='':
            return None
        return ls_temp[0]
    return None
def crawl_detail_seloger(text):
    soup = BeautifulSoup(text, 'html.parser')
    #city
    city = None
    try:
        city = soup.find('p', class_='localite').text
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get city', ex)
    # get picture
    picture = None
    try:
        div = soup.find("div", {"class": "carrousel_image"})
        soup1 = BeautifulSoup(str(div), 'html.parser')
        div = soup1.find_all("div", {"class": "carrousel_slide"})
        ls_rs = []
        for i in div:
            # get link in style
            link = str(i.get('style'))
            # clear link
            ls_temp = link.split('//')
            for y in ls_temp:
                if 'seloger.com' in y:
                    link = y.replace(');background-repeat:no-repeat;', '')
                    if 'https://' not in link:
                        link = 'https://' + link
                    ls_rs.append(link)
            # get link in data lazy
            link = str(i.get('data-lazy'))
            # clear link
            ls_temp = link.split('"')
            for y in ls_temp:
                if 'seloger.com' in y:
                    link = y.replace(');background-repeat:no-repeat;', '')
                    if 'https:' not in link:
                        link = 'https:' + link
                    ls_rs.append(link)
        ls_rs = list(set(ls_rs))
        picture = ''
        if ls_rs:
            for i in ls_rs:
                picture = picture + i + ', '
            picture = picture[:-2]
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get picture', ex)
        picture = None
    # print(picture)
    # get title
    title = None
    try:
        h1 = soup.find("h1", {"class": "detail-title title1"}).text.replace('\n', '')
        title = ' '.join(h1.split())
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get title', ex)
    # get description in text
    description = None
    try:
        div = soup.find("input", {"name": "description"})
        if div:
            description = str(div.get('value'))
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get description in text', ex)
    # print(description)
    # sleep(20)
    id_selector = None
    id_annonce = None
    id_agence = None
    id_tiers = None
    dpec = None
    dpel = None
    code_insee = None
    etage = None
    nb_chambres = None
    nb_pieces = None
    surface = None
    cp = None
    prix = None
    pro_Visibilite = None
    couplage = None
    type_transaction = None
    id_type_chauffage = None
    id_type_cuisine = None
    balcon = None
    eau = None
    bain = None
    mensualite = None
    id_publication = None
    id_tt = None
    id_type_bien = None
    type_bien = None
    tel_number = None
    object_mail = None
    intro_mail = None
    ville = None
    id_quartier = None
    nom_quartier = None
    try:
        script = soup.find_all('script', {'type': 'text/javascript'})
        for i in script:
            if 'idSelector' in str(i):
                script = i.text
                break
        # print(script)
        if text == None:
            item = {'id_selector': None, 'id_annonce': None, 'id_agence': None, 'id_tiers': None,
                    'dpec': None, 'dpel': None,
                    'code_insee': None, 'etage': None, 'nb_chambres': None, 'nb_pieces': None,
                    'surface': None, 'cp': None, 'prix': None, 'pro_Visibilite': None, 'couplage': None,
                    'type_transaction': None, 'id_type_chauffage': None,
                    'id_type_cuisine': None,
                    'balcon': None, 'eau': None, 'bain': None, 'mensualite': None,
                    'id_publication': None,
                    'id_tt': None, 'id_type_bien': None, 'type_bien': None, 'tel_number': None,
                    'object_mail': None, 'intro_mail': None, 'ville': None, 'id_quartier': None,
                    'nom_quartier': None}
            return item
        ls_temp = script.split(';')
        for i in ls_temp:
            if 'description' in i:
                continue
            if 'IdSelector' in i:
                id_selector = get_value(i)
            if 'idAnnonce' in i:
                id_annonce = get_value(i)
            if 'idAgence' in i:
                id_agence = get_value(i)
            if 'idTiers' in i:
                id_tiers = get_value(i)
            if 'dpeC' in i:
                dpec = get_value(i)
            if 'dpeL' in i:
                dpel = get_value(i)
            if 'codeInsee' in i:
                code_insee = get_value(i)
            if 'etage' in i:
                etage = get_value(i)
                if len(etage) > 50:
                    etage = None
            if 'nbChambres' in i:
                nb_chambres = get_value(i)
                if len(nb_chambres) > 50:
                    nb_chambres = None
            if 'nbPieces' in i:
                nb_pieces = get_value(i)
                if len(nb_pieces) > 50:
                    nb_pieces = None
            if 'surfaceT' in i:
                surface = get_value(i)
                if len(surface) > 50:
                    surface = None
            if 'cp' in i:
                cp = get_value(i)
            if 'prix' in i:
                prix = get_value(i)
                if len(prix) > 50:
                    prix = None
            if 'prodVisibilite' in i:
                pro_Visibilite = get_value(i)
            if 'couplage' in i:
                couplage = get_value(i)
            if 'typeTransaction' in i:
                type_transaction = get_value(i)
            if 'idTypeChauffage' in i:
                id_type_chauffage = get_value(i)
            if 'idTypeCuisine' in i:
                id_type_cuisine = get_value(i)
            if 'balcon' in i:
                balcon = get_value(i)
                if len(balcon)>50:
                    balcon = None
            if 'eau' in i:
                eau = get_value(i)
                if len(eau)>50:
                    eau = None
            if 'bain' in i:
                bain = get_value(i)
                if len(bain)>50:
                    bain = None
            if 'mensualite' in i:
                mensualite = get_value(i).replace(' ', '')
            if 'idPublication' in i:
                id_publication = get_value(i)
            if 'idTT' in i:
                id_tt = get_value(i)
            if 'idTypeBien' in i:
                id_type_bien = get_value(i)
            if 'typeBien' in i:
                type_bien = get_value(i)
            if 'telNumber' in i:
                tel_number = get_value(i)
                if len(tel_number) > 50:
                    tel_number = None
            if 'objectMail' in i:
                object_mail = get_value(i)
                if len(object_mail) > 50:
                    object_mail = None
            if 'introMail' in i:
                intro_mail = get_value(i)
            if 'ville' in i:
                ville = get_value(i)
            if 'idQuartier' in i:
                id_quartier = get_value(i)
            if 'nomQuartier' in i:
                nom_quartier = get_value(i)
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get script', ex)


    item = {'id_annonce': id_annonce, 'code_insee': None, 'title': object_mail, 'description': description, 'real_estate_type': type_bien,
            'transaction': type_transaction, 'price': prix, 'surface': surface, 'room': nb_pieces,
            'bedroom': nb_chambres, 'address': None, 'city': city, 'codepostal': None, 'picture': picture,
            'energy': dpel, 'energy_detail': dpec, 'etage': etage, 'type_cuisine': id_type_cuisine, 'balcon': balcon, 'water': eau,
            'bathroom': bain, 'mensualite': mensualite, 'tel_number': tel_number, 'region_name': None,
            'department_name': None, 'zipcode': None, 'urgent': None, 'date': None, 'charges_included': None,
            'floor': None, 'furnished': None, 'ges': None, 'ges_detail': None, 'updated': None, 'extra_infor': None,
            'web': 'seloger.com', 'insert_date': None}
    return item

def crawl_detail_seloger_construire(text):
    soup = BeautifulSoup(text, 'html.parser')
    title = None
    try:
        title = soup.find_all('a', class_="breadCrumbLink")[-1].text.replace('\n', '').replace('  ', '').replace('\r', '')
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get title', ex)
    city = None
    try:
        city = soup.find('span', class_="detailInfosCity").text.split()[0]
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get city', ex)
    description = None
    try:
        description = soup.find('div', class_="detailDescriptionContent").text.replace('\n', '').replace('    ', '')
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get description', ex)
    nb_bain = None
    nb_eau = None
    try:
        item = soup.find_all('li', class_="detailCaracteristiquesItem")
        nb_bain = None
        nb_eau = None
        for i in item:
            if 'bain' in i.text:
                nb_bain = i.text.replace('Salle de bain  : ', '').replace('\n', '').replace(' ', '')
            if 'eau' in i.text:
                nb_eau = i.text.replace("Salle d'eau  : ", '').replace('\n', '').replace(' ', '')

    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get city', ex)

    idannonce = None
    typedebien = None
    typedetransaction = None
    codepostal = None
    price = None
    surface = None
    surface_terrain = None
    photourl = None
    nbpieces = None
    nbchambres = None
    nbsalledebaim = None
    nbwc = None
    price_maison = None
    price_terrain = None
    try:
        script = soup.find('script', id="ava_data")
        y = json.loads(script.text[:-2].replace('  ava_data = ', '').replace(';', ''))
        idannonce = y['products'][0]['idannonce']
        typedebien = y['products'][0]['typedebien']
        typedetransaction = y['products'][0]['typedetransaction']
        codepostal = y['products'][0]['codepostal']
        price = y['products'][0]['prix']
        surface = y['products'][0]['surface']
        surface_terrain = y['products'][0]['surface_terrain']
        photourl = y['products'][0]['photourl']
        nbpieces = y['products'][0]['nbpieces']
        nbchambres = y['products'][0]['nbchambres']
        nbsalledebaim = y['products'][0]['nbsalledebaim']
        nbwc = y['products'][0]['nbwc']
        # price
        price_maison = None
        price_terrain = None
        prices = soup.find('div', class_='detailCaracteristiquesContentCol').text
        ls_prices = prices.split('\n')
        for i in ls_prices:
            if 'maison' in i:
                price_maison = get_price(i)
                pass
            if 'terrain' in i:
                price_terrain = get_price(i)
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get script', ex)

    item = {'id_annonce': idannonce, 'code_insee': None, 'title': title, 'real_estate_type': typedebien, 'transaction': typedetransaction,
            'codepostal': codepostal, 'price': price, 'surface': surface, 'surface_terrain': surface_terrain,
            'picture': photourl, 'room': nbpieces, 'bedroom': nbchambres, 'nbsalledebaim': nbsalledebaim,
            'nbwc': nbwc, 'price_maison': price_maison, 'price_terrain': price_terrain, 'city': city,
            'description': description, 'web': 'seloger-construire.com',
            'region_name':None,'department_name':None,
                 'zipcode': None,'urgent': None, 'date': None,'charges_included': None,
                 'floor': None, 'bathroom': None,'water': None,
                 'balcon': None,'furnished': None, 'ges': None,'ges_detail': None,'energy': None,
                 'energy_detail': None,'tel_number': None,'extra_infor': None,'updated': None,'insert_date': None}
    return item

def crawl_detail_selogerneuf(text):
    soup = BeautifulSoup(text, 'html.parser')
    # print(soup)
    price = None
    try:
        price = soup.find('p', class_="resumePrice summarySmall").text.replace('À partir de', '').replace(' ', ''). \
            replace('€', '').replace(' ', '').replace('\n', '').replace('\r', '')
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get price', ex)
    photourl = None
    try:
        photourl = soup.find('div', class_="detailCover fitCoverContainer").get('data-src')
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get photourl', ex)
    description = None
    try:
        description = soup.find('p', class_="detailDescriptionText summarySmall").text
        description = ' '.join(description[1:-1].split())
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get description', ex)
    city = None
    try:
        city = soup.find('address', class_="detailAddress summarySmall").text.replace('\n', '')
        city = ' '.join(city.split())
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get city', ex)
    title = None
    try:
        title = soup.find_all('p', class_="breadCrumbTitle wrapText")[-1].text.\
            replace('\n', '').replace('  ', '').replace('\r', '')
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get title', ex)
    address = None
    try:
        address = soup.find('address', class_="detailPOIAddress summarySmall").text.replace('\n', '')
        address = ' '.join(address.split())
    except Exception as ex:
        # logging.error('Error with URL check!')
        print('+ At SeLinkDetailAd.crawl_detail_ad() - get address', ex)
    idannonce = None
    typedebien = None
    typedetransaction = None
    codepostal = None
    surface = None
    surface_terrain = None
    nbpieces = None
    nbchambres = None
    nbsalledebaim = None
    nbwc = None
    price_maison = None
    price_terrain = None
    try:
        script = soup.find('script', id="ava_data")
        y = json.loads(script.text[:-2].replace('  ava_data = ', '').replace(';', ''))
        # print(y)

        idannonce = y['products'][0]['idannonce']
        typedebien = y['products'][0]['typedebien']
        typedetransaction = y['products'][0]['typedetransaction']
        codepostal = y['products'][0]['codepostal']
        surface = None
        surface_terrain = None
        nbpieces = None
        nbchambres = None
        nbsalledebaim = None
        nbwc = None
        price_maison = None
        price_terrain = None

    except Exception as ex:
            # logging.error('Error with URL check!')
            print('+ At SeLinkDetailAd.crawl_detail_ad() - get script', ex)

    item = {'id_annonce': idannonce, 'title': title, 'real_estate_type': typedebien, 'transaction': typedetransaction,
            'codepostal': codepostal, 'price': price, 'surface': surface, 'surface_terrain': surface_terrain,
            'picture': photourl, 'room': nbpieces, 'bedroom': nbchambres, 'nbsalledebaim': nbsalledebaim,
            'nbwc': nbwc, 'price_maison': price_maison, 'price_terrain': price_terrain,
            'description': description, 'code_insee': None,
            'address': address, 'city': city, 'web': 'selogerneuf.com',
            'region_name': None, 'department_name': None,
            'zipcode': None, 'urgent': None, 'date': None, 'charges_included': None,
            'floor': None, 'bathroom': None, 'water': None,
            'balcon': None, 'furnished': None, 'ges': None, 'ges_detail': None, 'energy': None,
            'energy_detail': None, 'tel_number': None, 'extra_infor': None, 'updated': None, 'insert_date': None}
    return item
if __name__ == '__main__':
    # f = open('seloger.com2.txt', 'r')
    # text = f.read()
    # item = crawl_detail_seloger(text)
    # for key, value in item.items():
    #     print(key, value)
    pass