#Introduce
- This service will support you to deploy your project in another PC. Just copy this service to your project, set database config, data, then run "init_system.sh". It's done.
# Require:
- requirements.txt at your root path (of your project)
- At deploy_service/data: mysql scripts backup (structure and data, just some tables having init data)
- At settings.py: set your config information.
# Run
In command line:
./init_system.sh
