from subprocess import call
from .settings import *
import os
import glob
import errno
from time import sleep
import pymysql
dir = os.getcwd()
print(dir)

def create_database():
    print('Start create database')
    try:
        mydb = pymysql.connect(
            host='localhost',
            user=MYSQL_USERNAME,
            password=MYSQL_PASSWORD,
        )
        cur = mydb.cursor()
        cur.execute('create database if not exists '+DATABASE_NAME+' charset = "utf8mb4"')
    except ValueError:
        print('Cannot create database. ', ValueError)

def restore_database():
    print('Start restore data')
    dir = os.getcwd()
    # path = dir + '/deploy_service/data/*.sql'
    path = './deploy_service/data/*.sql'
    files = glob.glob(path)
    for name in files:
        print('Restore:', name)
        call('mysql -u ' + MYSQL_USERNAME + ' -p' + MYSQL_PASSWORD + ' '+DATABASE_NAME+' < '+name, shell=True)


