
from model.models_remote import RealEstate as RealEstateRemote
from model.models import RealEstate as RealEstateLocal
from datetime import datetime, timedelta
from db_connection.mysql_ssh import db_remote, ssh
from audit_service.model.model import AuditJob, AuditStep

today = datetime.fromordinal(datetime.now().toordinal())
test = today - timedelta(days=1)
print(today)
print('Start ETL')
ses = RealEstateLocal.query.filter(RealEstateLocal.insert_date>=today).all()
print(len(ses))
job = AuditJob(name='Job: ETL remote seloger ' + str(datetime.now()))
job.start()
step = AuditStep(job, name = 'Step: ETL remote seloger ' + str(datetime.now()))
step.start()
step.update(sent_row=len(ses))
count = 0
count_delete = 0
for se in ses:
    # check already have or not
    # print(se.link_detail)
    se_remote = RealEstateRemote.query.filter(RealEstateRemote.link_detail==se.link_detail).first()
    if se_remote:
        if se_remote.compare(se):
            print('Already in db')
            continue
        else:
            print('Delete in remote', se_remote.id, se_remote.link_detail, 'Because of new update')
            db_remote.session.delete(se_remote)
            db_remote.session.commit()
            count_delete = count_delete + 1
    print('ETL:', se.id, se.link_detail)
    print('New ad')
    dict = se.__dict__
    del dict['_sa_instance_state']
    real_estate = RealEstateRemote()
    real_estate.set_atrr_by_dict(dict)
    # print(real_estate.__dict__)
    db_remote.session.add(real_estate)
    db_remote.session.commit()
    count = count + 1
step.update(received_row=count, comment = 'Number ads have updating: '+ str(count_delete))
step.end()
job.end()
print('Stop ssh')
ssh.stop()

