from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from settings import *

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+MYSQL_USERNAME+':'+MYSQL_PASSWORD+'@localhost/'+DATABASE_NAME
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
