from db_connection.mysql_local import db
from model.models import *
from model.models_suppoprt import SeTrackFilter
from datetime import datetime, date
from time import sleep
from db_connection.mysql_local import db

while 1:
    se_track_filter = SeTrackFilter.query.first()
    se_filters = SeFilter.query.filter(SeFilter.id >= se_track_filter.filter_id).all()
    max_filter = db.session.query(db.func.max(SeFilter.id)).scalar()
    # se_filters = SeFilter.query.all()
    for se_filter in se_filters:
        print('Filter:', se_filter.id)
        # track foot of filter, record the id of the filter which are crawling
        se_track_filter.filter_id = se_filter.id
        db.session.commit()
        # crawl link_detail insert into stag
        se_filter.crawl_link_detail_ad()
        # select stag => crawl detail insert into se_detail
        stag_se_link_detail_ads = StagSeLinkDetailAd.query.\
            filter(StagSeLinkDetailAd.filter_id == se_filter.id).\
            filter(StagSeLinkDetailAd.link_detail.like("%seloger%")).all()
        for stag_se_link_detail_ad in stag_se_link_detail_ads:
            if datetime.now().minute>=0 and datetime.now().minute<10:
                print("Sleep 600s to etl remote")
                sleep(600)
            if 'seloger' not in stag_se_link_detail_ad.link_detail:
                continue
            # crawl statistic
            crawl_statistic = CrawlStatistic.query.filter(CrawlStatistic.in_date==date.today()).first()
            if crawl_statistic == None:
                crawl_statistic = CrawlStatistic()
                db.session.add(crawl_statistic)
                db.session.commit()
                print('Create new crawl_statistic')
            crawl_statistic.NbTotalBeFetchedPlus()
            item_crawl = stag_se_link_detail_ad.crawl_detail_ad()
            if item_crawl == None:
                continue
            item_crawl['link_detail'] = stag_se_link_detail_ad.link_detail
            real_estate = RealEstate()
            real_estate.set_atrr_by_dict(item_crawl)
            real_estate_db = RealEstate.query.filter(RealEstate.link_detail==stag_se_link_detail_ad.link_detail).first()
            print('INsert date: ', real_estate.insert_date)
            if real_estate_db == None:
                print('New ad')
                crawl_statistic.NbNewPlus()
                db.session.add(real_estate)
                db.session.commit()
            else:
                if real_estate.compare(real_estate_db):
                    print('Already in db')
                    crawl_statistic.NbPassPlus()
                else:
                    crawl_statistic.NbUpdatePlus()
                    db.session.delete(real_estate_db)
                    db.session.commit()
                    db.session.add(real_estate)
                    db.session.commit()
                    print('Update (delete old, insert new)')

            # se_detail_ad = SeDetailAd(filter_id=se_filter.id, link_detail=stag_se_link_detail_ad.link_detail,
            #                           picture=item_crawl['picture'], category=item_crawl['category'],
            #                           price=item_crawl['price'], price_monthly=item_crawl['price_monthly'],
            #                           nb_room=item_crawl['nb_room'], nb_bedroom=item_crawl['nb_bedroom'],
            #                           extra_infor=item_crawl['extra_infor'], description=item_crawl['description'])
            # item = se_detail_ad.extract_extra_infor()
            # se_detail_ad_extracted = SeDetailAdExtracted(
            #     filter_id=se_filter.id, link_detail=stag_se_link_detail_ad.link_detail, picture=se_detail_ad.picture,
            #     id_selector=item['id_selector'],id_annonce=item['id_annonce'], id_agence=item['id_agence'],
            #     id_tiers=item['id_tiers'], dpec=item['dpec'], dpel=item['dpel'], code_insee=item['code_insee'],
            #     etage=item['etage'], nb_chambres=item['nb_chambres'], nb_pieces=item['nb_pieces'], surface=item['surface'],
            #     cp=item['cp'], prix=item['prix'], pro_Visibilite=item['pro_Visibilite'], couplage=item['couplage'],
            #     type_transaction=item['type_transaction'], id_type_chauffage=item['id_type_chauffage'],
            #     id_type_cuisine=item['id_type_cuisine'], balcon=item['balcon'], eau=item['eau'], bain=item['bain'],
            #     mensualite=item['mensualite'], id_publication=item['id_publication'], id_tt=item['id_tt'],
            #     id_type_bien=item['id_type_bien'], type_bien=item['type_bien'], tel_number=item['tel_number'],
            #     object_mail=item['object_mail'], intro_mail=item['intro_mail'], ville=item['ville'],
            #     id_quartier=item['id_quartier'], nom_quartier=item['nom_quartier'], description=item_crawl['description'],
            #     insert_date=se_detail_ad.insert_date)
            #
            # se_detail_ad_db = SeDetailAd.query.filter(SeDetailAd.link_detail==stag_se_link_detail_ad.link_detail).first()
            # se_detail_ad_extracted_db = SeDetailAdExtracted.query.filter(
            #     SeDetailAdExtracted.link_detail == stag_se_link_detail_ad.link_detail).first()
            # if se_detail_ad_db:
            #     if se_detail_ad_db.compare(se_detail_ad):
            #         continue
            #     else:
            #         db.session.delete(se_detail_ad_db)
            #         if se_detail_ad_extracted_db:
            #             db.session.delete(se_detail_ad_db)
            #         db.session.commit()
            # db.session.add(se_detail_ad_extracted)
            # db.session.add(se_detail_ad)
            # db.session.commit()

        StagSeLinkDetailAd.query.delete()
        db.session.commit()
        # start at the first filter if the last filter is crawled
        if se_filter.id == max_filter:
            se_track_filter.filter_id = 1
            db.session.commit()
        pass
