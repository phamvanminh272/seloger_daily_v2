from db_connection.mysql_local import db

class SeTrackFilter(db.Model):
    """
    Some time we will stop program for some reasons (may be to update source code). We need to know which filter we were
    crawling to start again (prevent starting crawling at the first filter). Then this class will help us to record the
    filter id.
    """
    __tablename__ = 'se_track_filter'
    id = db.Column(db.Integer, primary_key=True)
    filter_id = db.Column(db.Integer)

class SeCount(db.Model):
    __tablename__ = 'se_count'
    id = db.Column(db.Integer, primary_key=True)
    count = db.Column(db.Integer)

db.create_all()