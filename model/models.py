# encoding: utf-8
from db_connection.mysql_local import db
from tool.get_response import get_response
from tool.crawl_se import crawl_detail_selogerneuf, crawl_detail_seloger_construire, crawl_detail_seloger
from bs4 import BeautifulSoup
from datetime import datetime, date
import json

def clean_text(text):
    text = text[:-1]
    text = text[1:]
    return text

def get_value(string):
    ls_temp = string.split('"')
    ls_temp = ls_temp[-2:-1]
    if ls_temp:
        if ls_temp[0]=='':
            return None
        return ls_temp[0]
    return None

def get_price(text):
    CHAR = "abcdefghijklmnopqrstuvwxyzáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ € P:"
    for char in CHAR:
        # print(char)
        if char in text:
            text = text.replace(char, '')
    return text

# def crawling_seloger_detail(text):
#     soup0 = BeautifulSoup(text, 'html.parser')
#     # get picture
#     picture = None
#     try:
#         div = soup0.find("div", {"class": "carrousel_image"})
#         soup = BeautifulSoup(str(div), 'html.parser')
#         div = soup.find_all("div", {"class": "carrousel_slide"})
#         ls_rs = []
#         for i in div:
#             # get link in style
#             link = str(i.get('style'))
#             # clear link
#             ls_temp = link.split('//')
#             for y in ls_temp:
#                 if 'seloger.com' in y:
#                     link = y.replace(');background-repeat:no-repeat;', '')
#                     if 'https://' not in link:
#                         link = 'https://' + link
#                     ls_rs.append(link)
#             # get link in data lazy
#             link = str(i.get('data-lazy'))
#             # clear link
#             ls_temp = link.split('"')
#             for y in ls_temp:
#                 if 'seloger.com' in y:
#                     link = y.replace(');background-repeat:no-repeat;', '')
#                     if 'https:' not in link:
#                         link = 'https:' + link
#                     ls_rs.append(link)
#         ls_rs = list(set(ls_rs))
#         picture = ''
#         if ls_rs:
#             for i in ls_rs:
#                 picture = picture + i + ', '
#             picture = picture[:-2]
#     except Exception as ex:
#         # logging.error('Error with URL check!')
#         print('+ At SeLinkDetailAd.crawl_detail_ad() - get picture', ex)
#         picture = None
#
#     # get title
#     title = None
#     try:
#         h1 = soup0.find("h1", {"class": "detail-title title1"})
#         if h1:
#             title = clean_text(h1.text).replace('\n', '')
#             w = 1
#             while w == 1:
#                 if title[0] == ' ':
#                     title = title[1:]
#                 else:
#                     w = 0
#     except Exception as ex:
#         # logging.error('Error with URL check!')
#         print('+ At SeLinkDetailAd.crawl_detail_ad() - get title', ex)
#
#     # get criteria
#     category = None
#     nb_room = None
#     nb_bedroom = None
#     price = None
#     price_monthly = None
#     try:
#         div = soup0.find_all("div", {"class": "resume"})
#         for d in div:
#             if 'criterion' in str(d):
#                 soup = BeautifulSoup(str(d), 'html.parser')
#                 # get category
#                 h2 = soup.find("h2", {"class": "c-h2"})
#                 if h2:
#                     category = h2.text
#                 # get nb_room
#                 li = soup.find_all("li")
#                 if li:
#                     for i in li:
#                         if 'pièces' in str(i):
#                             nb_room = i.text
#                             nb_room = nb_room.replace(' pièces', '').replace(' pièce', '')
#                 # get nb_bedroom
#                 li = soup.find_all("li")
#                 if li:
#                     for i in li:
#                         if 'chambre' in str(i):
#                             nb_bedroom = i.text
#                             nb_bedroom = nb_bedroom.replace(' chambre', '').replace('s', '')
#                 # get area
#                 li = soup.find_all("li")
#                 if li:
#                     for i in li:
#                         if 'm²' in str(i):
#                             area = i.text
#                             area = area.replace(' m²', '')
#                 # get location
#                 p = soup.find("p", {"class": "localite"})
#                 if p:
#                     location = p.text
#                 # get price
#                 a = soup.find("a", {"id": "price"})
#                 if a:
#                     price = a.text
#                     price = price.replace(' ', '').replace('\n', '').replace('\xa0', '').replace('\r', '')
#                 # get price monthly
#                 a = soup.find("a", {"class": "monthly"})
#                 if a:
#                     price_monthly = a.text
#                     price_monthly = price_monthly.replace('\xa0', '').replace('\r', '')
#                 # get ref
#                 p = soup.find("p", {"class": "ref"})
#                 if p:
#                     ref = p.text
#                     ref = ref.replace('Réf: ', '')
#     except Exception as ex:
#         # logging.error('Error with URL check!')
#         print('+ At SeLinkDetailAd.crawl_detail_ad() - get criteria', ex)
#
#     # get description in text
#     description = None
#     try:
#         div = soup0.find("input", {"name": "description"})
#         if div:
#             description = str(div.get('value'))
#     except Exception as ex:
#         # logging.error('Error with URL check!')
#         print('+ At SeLinkDetailAd.crawl_detail_ad() - get description in text', ex)
#
#     # get detail
#     extra_infor = None
#     try:
#         script = soup0.find_all('script', {'type': 'text/javascript'})
#         for i in script:
#             if 'idSelector' in str(i):
#                 # print(i)
#                 ls_temp = str(i).split('/*')
#                 extra_infor = ''
#                 for y in ls_temp[:-2]:
#                     if 'Object.defineProperty( ConfigDetail' in y:
#                         extra_infor = extra_infor + y.replace('{', '').replace('})', '').replace('\n', '').replace(
#                             """Object.defineProperty( ConfigDetail""", '').replace('        ', '').replace(
#                             ',  enumerable: true;', '').replace(' string}', '').replace(';', '').replace(
#                             '\r', '').replace('*/', '').replace('\*', '').replace(',  enumerable: true',
#                                                                                   '').replace(
#                             '\\', '') + '; '
#                 extra_infor = extra_infor[:-2]
#
#     except Exception as ex:
#         # logging.error('Error with URL check!')
#         print('+ At SeLinkDetailAd.crawl_detail_ad() - get detail', ex)
#
#     item = {'description': description, 'picture': picture, 'category': category, 'price': price,
#             'price_monthly': price_monthly,
#             'nb_room': nb_room, 'nb_bedroom': nb_bedroom, 'extra_infor': extra_infor}
#     return item

# def crawling_seloger_construire_detail(text):
#     soup = BeautifulSoup(text, 'html.parser')
#     title = soup.find_all('a', class_="breadCrumbLink")[-1].text.replace('\n', '').replace('  ', '').replace('\r', '')
#     city = soup.find('span', class_="detailInfosCity").text.split()[0]
#     description = soup.find('div', class_="detailDescriptionContent").text.replace('\n', '').replace('    ', '')
#     item = soup.find_all('li', class_="detailCaracteristiquesItem")
#
#     nb_bain = None
#     nb_eau = None
#     for i in item:
#         if 'bain' in i.text:
#             nb_bain = i.text.replace('Salle de bain  : ', '').replace('\n', '').replace(' ', '')
#         if 'eau' in i.text:
#             nb_eau = i.text.replace("Salle d'eau  : ", '').replace('\n', '').replace(' ', '')
#
#     script = soup.find('script', id="ava_data")
#     y = json.loads(script.text[:-2].replace('  ava_data = ', '').replace(';', ''))
#     idannonce = y['products'][0]['idannonce']
#     typedebien = y['products'][0]['typedebien']
#     typedetransaction = y['products'][0]['typedetransaction']
#     codepostal = y['products'][0]['codepostal']
#     price = y['products'][0]['prix']
#     surface = y['products'][0]['surface']
#     surface_terrain = y['products'][0]['surface_terrain']
#     photourl = y['products'][0]['photourl']
#     nbpieces = y['products'][0]['nbpieces']
#     nbchambres = y['products'][0]['nbchambres']
#     nbsalledebaim = y['products'][0]['nbsalledebaim']
#     nbwc = y['products'][0]['nbwc']
#     # price
#     price_maison = None
#     price_terrain = None
#     prices = soup.find('div', class_='detailCaracteristiquesContentCol').text
#     ls_prices = prices.split('\n')
#     for i in ls_prices:
#         if 'maison' in i:
#             price_maison = get_price(i)
#             pass
#         if 'terrain' in i:
#             price_terrain = get_price(i)
#
#     item = {'idannonce': idannonce, 'title': title, 'typedebien': typedebien, 'typedetransaction': typedetransaction,
#             'codepostal': codepostal, 'price': price, 'surface': surface, 'surface_terrain': surface_terrain,
#             'photourl': photourl, 'nbpieces': nbpieces, 'nbchambres': nbchambres, 'nbsalledebaim': nbsalledebaim,
#             'nbwc': nbwc, 'price_maison': price_maison, 'price_terrain': price_terrain, 'city': city,
#             'web': 'seloger-construire.com'}
#     # print(item)
#     return item


class SeFilter(db.Model):
    __tablename__ = 'se_filter'
    id = db.Column(db.Integer, primary_key=True)
    category_value = db.Column(db.String(45))
    project_value = db.Column(db.String(45))
    department_value = db.Column(db.String(45))
    enterprise_value = db.Column(db.String(45))
    link_filter = db.Column(db.String(250))

    def generate_link(self):
        """
        make link filter with paramaters are attribute of the self
        :return: url (string)
        """
        url = 'https://www.seloger.com/list.htm?'
        if self.category_value != None:
            url = url + 'types=' + self.category_value
        if self.project_value != None:
            url = url + '&projects=' + self.project_value
        if self.enterprise_value != None:
            url = url + '&enterprise=' + self.enterprise_value
        if self.department_value != None:
            url = url + '&places=[{cp:' + self.department_value + '}]&qsVersion=1.0'
        else:
            url = url + '&places=[{div:2238}]&qsVersion=1.0'
        return url

    def crawl_link_detail_ad(self):
        """
        :return:
        """
        max_nb_dup = 5
        nb_dup=0
        nb_page = 0
        while nb_dup<=max_nb_dup:
            nb_page = nb_page+1
            link_root = self.link_filter + '&LISTING-LISTpg=' + str(nb_page)
            ls_link_detail = []
            data = get_response(link_root)['text']
            if data is None:
                continue
            # start to get link here
            soup = BeautifulSoup(data, 'html.parser')
            d = soup.findAll("div", {"class": "c-wrap-main"})
            soup_child = BeautifulSoup(str(d), 'html.parser')
            for a in soup_child.findAll("a"):
                # get link
                link_detail = str(a.get('href'))
                # print(link_detail)
                if 'com/annonces' in link_detail:
                    ls_temp = link_detail.split('?')
                    link_detail = ls_temp[0]
                    # print("---------", link_detail)
                    ls_link_detail.append(link_detail)
            ls_link_detail=list(set(ls_link_detail))
            # end crawling links
            if ls_link_detail:
                for i in ls_link_detail:
                    if RealEstate.query.filter(RealEstate.link_detail==i).first():
                        nb_dup = nb_dup + 1
                    else:
                        nb_dup = 0
                    if nb_dup >=max_nb_dup:
                        print('Stop crawling list of links. Because too much ads have already in db.')
                        continue
                    stag_se_link_detail_ad = StagSeLinkDetailAd(filter_id=self.id, link_detail=i)
                    db.session.add(stag_se_link_detail_ad)
                    db.session.commit()
            else:
                # do have any ad, stop loop by change 'nb_dup'
                nb_dup = 10
            if nb_page >= 3:
                # nb_dup = 10
                print('Stop crawling list of links. Because number_page > 3.')
                break

class StagSeLinkDetailAd(db.Model):
    __tablename__ = 'stag_se_link_detail_ad'
    id = db.Column(db.Integer, primary_key=True)
    filter_id = db.Column(db.Integer)
    link_detail = db.Column(db.String(250))
    insert_date = db.Column(db.DateTime)

    def __init__(self, filter_id=None, link_detail=None, insert_date=datetime.now()):
        self.filter_id = filter_id
        self.link_detail = link_detail
        self.insert_date = insert_date

    def crawl_detail_ad(self):
        print('Crawl detail ad')
        data = get_response(self.link_detail)
        # checking is link valid?
        if data['status_code']:
            text = data['text']
        else:
            return None
        if 'seloger.com' in data['url']:
            item = crawl_detail_seloger(data['text'])
            return item
        if 'seloger-con' in data['url']:
            item = crawl_detail_seloger_construire(data['text'])
            return item
        if 'selogerneu' in data['url']:
            item = crawl_detail_selogerneuf(data['text'])
            return item
        return None
        # soup0 = BeautifulSoup(text, 'html.parser')
        #
        # # get picture
        # picture = None
        # try:
        #     div = soup0.find("div", {"class": "carrousel_image"})
        #     soup = BeautifulSoup(str(div), 'html.parser')
        #     div = soup.find_all("div", {"class": "carrousel_slide"})
        #     ls_rs = []
        #     for i in div:
        #         # get link in style
        #         link = str(i.get('style'))
        #         # clear link
        #         ls_temp = link.split('//')
        #         for y in ls_temp:
        #             if 'seloger.com' in y:
        #                 link = y.replace(');background-repeat:no-repeat;', '')
        #                 if 'https://' not in link:
        #                     link = 'https://' + link
        #                 ls_rs.append(link)
        #         # get link in data lazy
        #         link = str(i.get('data-lazy'))
        #         # clear link
        #         ls_temp = link.split('"')
        #         for y in ls_temp:
        #             if 'seloger.com' in y:
        #                 link = y.replace(');background-repeat:no-repeat;', '')
        #                 if 'https:' not in link:
        #                     link = 'https:' + link
        #                 ls_rs.append(link)
        #     ls_rs = list(set(ls_rs))
        #     picture = ''
        #     if ls_rs:
        #         for i in ls_rs:
        #             picture = picture + i + ', '
        #         picture = picture[:-2]
        # except Exception as ex:
        #     # logging.error('Error with URL check!')
        #     print('+ At SeLinkDetailAd.crawl_detail_ad() - get picture', ex)
        #     picture = None
        #
        # # get title
        # title = None
        # try:
        #     h1 = soup0.find("h1", {"class": "detail-title title1"})
        #     if h1:
        #         title = clean_text(h1.text).replace('\n', '')
        #         w = 1
        #         while w == 1:
        #             if title[0] == ' ':
        #                 title = title[1:]
        #             else:
        #                 w = 0
        # except Exception as ex:
        #     # logging.error('Error with URL check!')
        #     print('+ At SeLinkDetailAd.crawl_detail_ad() - get title', ex)
        #
        # # get criteria
        # category = None
        # nb_room = None
        # nb_bedroom = None
        # price = None
        # price_monthly = None
        # try:
        #     div = soup0.find_all("div", {"class": "resume"})
        #     for d in div:
        #         if 'criterion' in str(d):
        #             soup = BeautifulSoup(str(d), 'html.parser')
        #             # get category
        #             h2 = soup.find("h2", {"class": "c-h2"})
        #             if h2:
        #                 category = h2.text
        #             # get nb_room
        #             li = soup.find_all("li")
        #             if li:
        #                 for i in li:
        #                     if 'pièces' in str(i):
        #                         nb_room = i.text
        #                         nb_room = nb_room.replace(' pièces', '').replace(' pièce', '')
        #             # get nb_bedroom
        #             li = soup.find_all("li")
        #             if li:
        #                 for i in li:
        #                     if 'chambre' in str(i):
        #                         nb_bedroom = i.text
        #                         nb_bedroom = nb_bedroom.replace(' chambre', '').replace('s', '')
        #             # get area
        #             li = soup.find_all("li")
        #             if li:
        #                 for i in li:
        #                     if 'm²' in str(i):
        #                         area = i.text
        #                         area = area.replace(' m²', '')
        #             # get location
        #             p = soup.find("p", {"class": "localite"})
        #             if p:
        #                 location = p.text
        #             # get price
        #             a = soup.find("a", {"id": "price"})
        #             if a:
        #                 price = a.text
        #                 price = price.replace(' ', '').replace('\n', '').replace('\xa0', '').replace('\r', '')
        #             # get price monthly
        #             a = soup.find("a", {"class": "monthly"})
        #             if a:
        #                 price_monthly = a.text
        #                 price_monthly = price_monthly.replace('\xa0', '').replace('\r', '')
        #             # get ref
        #             p = soup.find("p", {"class": "ref"})
        #             if p:
        #                 ref = p.text
        #                 ref = ref.replace('Réf: ', '')
        # except Exception as ex:
        #     # logging.error('Error with URL check!')
        #     print('+ At SeLinkDetailAd.crawl_detail_ad() - get criteria', ex)
        #
        # # get description in text
        # description = None
        # try:
        #     div = soup0.find("input", {"name": "description"})
        #     if div:
        #         description = str(div.get('value'))
        # except Exception as ex:
        #     # logging.error('Error with URL check!')
        #     print('+ At SeLinkDetailAd.crawl_detail_ad() - get description in text', ex)
        #
        # # get detail
        # extra_infor = None
        # try:
        #     script = soup0.find_all('script', {'type': 'text/javascript'})
        #     for i in script:
        #         if 'idSelector' in str(i):
        #             # print(i)
        #             ls_temp = str(i).split('/*')
        #             extra_infor = ''
        #             for y in ls_temp[:-2]:
        #                 if 'Object.defineProperty( ConfigDetail' in y:
        #                     extra_infor = extra_infor + y.replace('{', '').replace('})', '').replace('\n', '').replace(
        #                         """Object.defineProperty( ConfigDetail""", '').replace('        ', '').replace(
        #                         ',  enumerable: true;', '').replace(' string}', '').replace(';', '').replace(
        #                         '\r', '').replace('*/', '').replace('\*', '').replace(',  enumerable: true',
        #                                                                               '').replace(
        #                         '\\', '') + '; '
        #             extra_infor = extra_infor[:-2]
        #
        # except Exception as ex:
        #     # logging.error('Error with URL check!')
        #     print('+ At SeLinkDetailAd.crawl_detail_ad() - get detail', ex)
        #
        # item = {'description': description, 'picture': picture, 'category': category, 'price': price, 'price_monthly': price_monthly,
        #         'nb_room': nb_room, 'nb_bedroom': nb_bedroom, 'extra_infor': extra_infor}
        # return item

class CrawlStatistic(db.Model):
    __tablename__ = 'crawl_statistic'
    id = db.Column(db.Integer, primary_key=True)
    in_date = db.Column(db.DateTime)
    nb_pass = db.Column(db.Integer) # nb ad have already had in db, but do not have any change.
    nb_update = db.Column(db.Integer) # nb ad have updated (Delete old, insert new)
    nb_new = db.Column(db.Integer) # nb new ad
    nb_total_be_fetched = db.Column(db.Integer)

    def __init__(self, in_date=date.today(), nb_pass=0, nb_update=0, nb_new=0, nb_total_be_fetched=0):
        self.in_date = in_date
        self.nb_pass = nb_pass
        self.nb_update = nb_update
        self.nb_new = nb_new
        self.nb_total_be_fetched = nb_total_be_fetched

    def NbPassPlus(self, add_nb_pass=1):
        self.nb_pass = self.nb_pass + add_nb_pass
        db.session.commit()

    def NbUpdatePlus(self, add_nb_update=1):
        self.nb_update = self.nb_update + add_nb_update
        db.session.commit()

    def NbNewPlus(self, add_nb_new=1):
        self.nb_new = self.nb_new + add_nb_new
        db.session.commit()
    def NbTotalBeFetchedPlus(self, add_nb_total_be_fetched=1):
        self.nb_total_be_fetched = self.nb_total_be_fetched + add_nb_total_be_fetched
        db.session.commit()

class RealEstate(db.Model):
    __tablename__ = 'real_estate'
    id = db.Column(db.Integer, primary_key=True)
    link_detail = db.Column(db.String(250))
    id_annonce = db.Column(db.String(50))
    code_insee = db.Column(db.String(100))
    title = db.Column(db.String(250))
    description = db.Column(db.Text)
    transaction = db.Column(db.String(50))
    picture = db.Column(db.Text)
    region_name = db.Column(db.String(75))
    department_name = db.Column(db.String(75))
    city = db.Column(db.String(75))
    zipcode = db.Column(db.String(75))
    urgent = db.Column(db.String(50))
    price = db.Column(db.String(100))
    date = db.Column(db.String(75))
    charges_included = db.Column(db.String(250))
    real_estate_type = db.Column(db.String(100))
    floor = db.Column(db.String(75))
    room = db.Column(db.String(50))
    bedroom = db.Column(db.String(50))
    bathroom = db.Column(db.String(50))
    water = db.Column(db.String(50))
    balcon = db.Column(db.String(75))
    furnished = db.Column(db.String(50))
    surface = db.Column(db.String(100))
    ges = db.Column(db.String(100))
    ges_detail = db.Column(db.String(100))
    energy = db.Column(db.String(100))
    energy_detail = db.Column(db.String(100))
    tel_number = db.Column(db.String(100))
    extra_infor = db.Column(db.Text)
    updated = db.Column(db.String(100))
    insert_date = db.Column(db.DateTime)
    web = db.Column(db.String(100))

    def __init__(self, link_detail=None, id_annonce=None, code_insee=None, title=None, description=None,
                 transaction=None, picture=None, region_name=None, department_name=None, city = None,
                 zipcode = None, urgent = None, price = None, date = None, charges_included = None,
                 real_estate_type = None, floor = None, room = None, bedroom = None, bathroom = None, water= None,
                 balcon = None, furnished = None, surface = None, ges = None, ges_detail = None, energy = None,
                 energy_detail = None, tel_number = None, extra_infor = None, updated = None, insert_date = None,
                 web = None):
        self.link_detail = link_detail
        self.id_annonce = id_annonce
        self.code_insee = code_insee
        self.title = title
        self.description = description
        self.transaction = transaction
        self.picture = picture
        self.region_name = region_name
        self.department_name = department_name
        self.city = city
        self.zipcode = zipcode
        self.urgent = urgent
        self.price = price
        self.date = date
        self.charges_included = charges_included
        self.real_estate_type = real_estate_type
        self.floor = floor
        self.room = room
        self.bedroom = bedroom
        self.bathroom = bathroom
        self.water = water
        self.balcon = balcon
        self.furnished = furnished
        self.surface = surface
        self.ges = ges
        self.ges_detail = ges_detail
        self. energy = energy
        self. energy_detail = energy_detail
        self.tel_number = tel_number
        self.extra_infor = extra_infor
        self.updated = updated
        self.insert_date = insert_date
        self.web = web
    def set_atrr_by_dict(self, dict):
        # ls_attr = [self.link_detail, self.id_annonce, self.code_insee, self.title, self.description,
        #          self.transaction, self.picture, self.region_name, self.department_name, self.city,
        #          self.zipcode, self.urgent, self.price, self.date, self.charges_included,
        #          self.real_estate_type, self.floor, self.room, self.bedroom, self.bathroom, self.water,
        #          self.balcon, self.furnished, self.surface, self.ges, self.ges_detail, self.energy,
        #          self.energy_detail, self.tel_number, self.extra_infor, self.updated, self.insert_date,
        #          self.web]
        ls_attr_name = ['link_detail', 'id_annonce', 'code_insee', 'title', 'description',
        'transaction', 'picture', 'region_name', 'department_name', 'city',
        'zipcode', 'urgent', 'price', 'date', 'charges_included',
        'real_estate_type', 'floor', 'room', 'bedroom', 'bathroom', 'water',
        'balcon', 'furnished', 'surface', 'ges', 'ges_detail', 'energy',
        'energy_detail', 'tel_number', 'extra_infor', 'updated', 'insert_date',
        'web']
        extra_infor = ''
        for key, value in dict.items():
            if key not in ls_attr_name and value != None:
                temp = "'"+key+"': "+value+"; "
                extra_infor = extra_infor + temp
        # print(extra_infor[:-1])
        # ls = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self, a))]
        # print(ls)
        # for index in range(len(ls_attr_name)):
        #     if ls_attr_name[index] in dict:
        #         ls_attr[index] = dict[ls_attr_name[index]]
        # self_dict = self.__dict__
        # for key, value in self_dict.items():
        #     print(key, value)
        self.link_detail = dict['link_detail']
        self.id_annonce = dict['id_annonce']
        self.code_insee = dict['code_insee']
        self.title = dict['title']
        self.description = dict['description']
        self.transaction = dict['transaction']
        self.picture = dict['picture']
        self.region_name = dict['region_name']
        self.department_name = dict['department_name']
        self.city = dict['city']
        self.zipcode = dict['zipcode']
        self.urgent = dict['urgent']
        self.price = dict['price']
        self.date = dict['date']
        self.charges_included = dict['charges_included']
        self.real_estate_type = dict['real_estate_type']
        self.floor = dict['floor']
        self.room = dict['room']
        self.bedroom = dict['bedroom']
        self.bathroom = dict['bathroom']
        self.water = dict['water']
        self.balcon = dict['balcon']
        self.furnished = dict['furnished']
        self.surface = dict['surface']
        self.ges = dict['ges']
        self.ges_detail = dict['ges_detail']
        self.energy = dict['energy']
        self.energy_detail = dict['energy_detail']
        self.tel_number = dict['tel_number']
        if extra_infor != '':
            self.extra_infor = extra_infor
        else:
            self.extra_infor = dict['description']
        self.updated = dict['updated']
        self.insert_date = datetime.now()
        self.web = dict['web']
    def compare(self, other):
        other_dict = other.__dict__
        del other_dict['_sa_instance_state']
        del other_dict['id']
        del other_dict['insert_date']
        self_dict = self.__dict__
        del self_dict['_sa_instance_state']
        if 'id' in self_dict:
            del self_dict['id']
        del self_dict['insert_date']
        if other_dict == self_dict:
            return True
        return True

# class SeDetailAd(db.Model):
#     __tablename__ = 'se_detail_ad'
#     id = db.Column(db.Integer, primary_key=True)
#     filter_id = db.Column(db.Integer)
#     link_detail = db.Column(db.String(250))
#     picture = db.Column(db.Text)
#     category = db.Column(db.String(50))
#     city = db.Column(db.String(50))
#     price = db.Column(db.String(50))
#     price_monthly = db.Column(db.String(75))
#     nb_room = db.Column(db.String(50))
#     nb_bedroom = db.Column(db.String(50))
#     extra_infor = db.Column(db.Text)
#     description = db.Column(db.Text)
#     insert_date = db.Column(db.DateTime)
#
#     def __init__(self, filter_id=None, link_detail=None, picture=None, category=None, city=None, price=None,
#                  price_monthly=None, nb_room=None, nb_bedroom=None, extra_infor=None, description=None,
#                  insert_date=datetime.now()):
#         self.filter_id = filter_id
#         self.link_detail = link_detail
#         self.picture = picture
#         self.category = category
#         self.city = city
#         self.price = price
#         self.price_monthly = price_monthly
#         self.nb_room = nb_room
#         self.nb_bedroom = nb_bedroom
#         self.extra_infor = extra_infor
#         self.description = description
#         self.insert_date = insert_date
#
#     def compare(self, se_detail_ad):
#         if self.link_detail == se_detail_ad.link_detail and self.picture == se_detail_ad.picture \
#                 and self.extra_infor == se_detail_ad.extra_infor and self.description==se_detail_ad.description:
#             return True
#         else:
#             return False
#     def extract_extra_infor(self):
#         text = self.extra_infor
#         if text == None:
#             item = {'id_selector': None, 'id_annonce': None, 'id_agence': None, 'id_tiers': None,
#                     'dpec': None, 'dpel': None,
#                     'code_insee': None, 'etage': None, 'nb_chambres': None, 'nb_pieces': None,
#                     'surface': None, 'cp': None, 'prix': None, 'pro_Visibilite': None, 'couplage': None,
#                     'type_transaction': None, 'id_type_chauffage': None,
#                     'id_type_cuisine': None,
#                     'balcon': None, 'eau': None, 'bain': None, 'mensualite': None,
#                     'id_publication': None,
#                     'id_tt': None, 'id_type_bien': None, 'type_bien': None, 'tel_number': None,
#                     'object_mail': None, 'intro_mail': None, 'ville': None, 'id_quartier': None,
#                     'nom_quartier': None}
#             return item
#         ls_temp = text.split(';')
#         id_selector = None
#         id_annonce = None
#         id_agence = None
#         id_tiers = None
#         dpec = None
#         dpel = None
#         code_insee = None
#         etage = None
#         nb_chambres = None
#         nb_pieces = None
#         surface = None
#         cp = None
#         prix = None
#         pro_Visibilite = None
#         couplage = None
#         type_transaction = None
#         id_type_chauffage = None
#         id_type_cuisine = None
#         balcon = None
#         eau = None
#         bain = None
#         mensualite = None
#         id_publication = None
#         id_tt = None
#         id_type_bien = None
#         type_bien = None
#         tel_number = None
#         object_mail = None
#         intro_mail = None
#         ville = None
#         id_quartier = None
#         nom_quartier = None
#         for i in ls_temp:
#             if 'IdSelector' in i:
#                 id_selector = get_value(i)
#             if 'idAnnonce' in i:
#                 id_annonce = get_value(i)
#             if 'idAgence' in i:
#                 id_agence = get_value(i)
#             if 'idTiers' in i:
#                 id_tiers = get_value(i)
#             if 'dpeC' in i:
#                 dpec = get_value(i)
#             if 'dpeL' in i:
#                 dpel = get_value(i)
#             if 'codeInsee' in i:
#                 code_insee = get_value(i)
#             if 'etage' in i:
#                 etage = get_value(i)
#             if 'nbChambres' in i:
#                 nb_chambres = get_value(i)
#             if 'nbPieces' in i:
#                 nb_pieces = get_value(i)
#             if 'surfaceT' in i:
#                 surface = get_value(i)
#             if 'cp' in i:
#                 cp = get_value(i)
#             if 'prix' in i:
#                 prix = get_value(i)
#             if 'prodVisibilite' in i:
#                 pro_Visibilite = get_value(i)
#             if 'couplage' in i:
#                 couplage = get_value(i)
#             if 'typeTransaction' in i:
#                 type_transaction = get_value(i)
#             if 'idTypeChauffage' in i:
#                 id_type_chauffage = get_value(i)
#             if 'idTypeCuisine' in i:
#                 id_type_cuisine = get_value(i)
#             if 'balcon' in i:
#                 balcon = get_value(i)
#             if 'eau' in i:
#                 eau = get_value(i)
#             if 'bain' in i:
#                 bain = get_value(i)
#             if 'mensualite' in i:
#                 mensualite = get_value(i)
#             if 'idPublication' in i:
#                 id_publication = get_value(i)
#             if 'idTT' in i:
#                 id_tt = get_value(i)
#             if 'idTypeBien' in i:
#                 id_type_bien = get_value(i)
#             if 'typeBien' in i:
#                 type_bien = get_value(i)
#             if 'telNumber' in i:
#                 tel_number = get_value(i)
#             if 'objectMail' in i:
#                 object_mail = get_value(i)
#             if 'introMail' in i:
#                 intro_mail = get_value(i)
#             if 'ville' in i:
#                 ville = get_value(i)
#             if 'idQuartier' in i:
#                 id_quartier = get_value(i)
#             if 'nomQuartier' in i:
#                 nom_quartier = get_value(i)
#
#         item = {'id_selector': id_selector, 'id_annonce': id_annonce, 'id_agence': id_agence, 'id_tiers': id_tiers,
#                 'dpec': dpec, 'dpel': dpel,
#                 'code_insee': code_insee, 'etage': etage, 'nb_chambres': nb_chambres, 'nb_pieces': nb_pieces,
#                 'surface': surface, 'cp': cp, 'prix': prix, 'pro_Visibilite': pro_Visibilite, 'couplage': couplage,
#                 'type_transaction': type_transaction, 'id_type_chauffage': id_type_chauffage,
#                 'id_type_cuisine': id_type_cuisine,
#                 'balcon': balcon, 'eau': eau, 'bain': bain, 'mensualite': mensualite, 'id_publication': id_publication,
#                 'id_tt': id_tt, 'id_type_bien': id_type_bien, 'type_bien': type_bien, 'tel_number': tel_number,
#                 'object_mail': object_mail, 'intro_mail': intro_mail, 'ville': ville, 'id_quartier': id_quartier,
#                 'nom_quartier': nom_quartier}
#         return item
#
# class SeDetailAdExtracted(db.Model):
#     __tablename__ = 'se_detail_ad_extracted'
#     id = db.Column(db.Integer, primary_key=True)
#     filter_id = db.Column(db.Integer)
#     link_detail = db.Column(db.String(250))
#     picture = db.Column(db.Text)
#     id_selector = db.Column(db.String(100))
#     id_annonce = db.Column(db.String(100))
#     id_agence = db.Column(db.String(100))
#     id_tiers = db.Column(db.String(100))
#     dpec = db.Column(db.String(100))
#     dpel = db.Column(db.String(100))
#     code_insee = db.Column(db.String(100))
#     etage = db.Column(db.String(100))
#     nb_chambres = db.Column(db.String(100))
#     nb_pieces = db.Column(db.String(100))
#     surface = db.Column(db.String(100))
#     cp = db.Column(db.String(100))
#     prix = db.Column(db.String(100))
#     pro_Visibilite = db.Column(db.String(100))
#     couplage = db.Column(db.String(100))
#     type_transaction = db.Column(db.String(100))
#     id_type_chauffage = db.Column(db.String(100))
#     id_type_cuisine = db.Column(db.String(100))
#     balcon = db.Column(db.String(75))
#     eau = db.Column(db.String(100))
#     bain = db.Column(db.String(100))
#     mensualite = db.Column(db.String(100))
#     id_publication = db.Column(db.String(100))
#     id_tt = db.Column(db.String(100))
#     id_type_bien = db.Column(db.String(100))
#     type_bien = db.Column(db.String(100))
#     tel_number = db.Column(db.String(100))
#     object_mail = db.Column(db.String(100))
#     intro_mail = db.Column(db.String(100))
#     ville = db.Column(db.String(100))
#     id_quartier = db.Column(db.String(100))
#     nom_quartier = db.Column(db.String(100))
#     description = db.Column(db.Text)
#     insert_date = db.Column(db.DateTime)
#
#     def __init__(self, filter_id=None, link_detail=None, picture=None, id_selector=None,id_annonce=None,
#                  id_agence=None, id_tiers=None, dpec=None, dpel=None, code_insee=None, etage=None,
#                  nb_chambres=None, nb_pieces=None, surface=None, cp=None, prix=None, pro_Visibilite=None,
#                  couplage=None, type_transaction=None, id_type_chauffage=None, id_type_cuisine=None,
#                  balcon=None, eau=None, bain=None, mensualite=None, id_publication=None, id_tt=None,
#                  id_type_bien=None, type_bien=None, tel_number = None, object_mail=None, intro_mail=None,
#                  ville = None, id_quartier=None, nom_quartier=None, description=None, insert_date=None):
#         self.filter_id = filter_id
#         self.link_detail = link_detail
#         self.picture = picture
#         self.id_selector = id_selector
#         self.id_annonce = id_annonce
#         self.id_agence = id_agence
#         self.id_tiers = id_tiers
#         self.dpec = dpec
#         self.dpel = dpel
#         self.code_insee = code_insee
#         self.etage = etage
#         self.nb_chambres = nb_chambres
#         self.nb_pieces = nb_pieces
#         self.surface = surface
#         self.cp = cp
#         self.prix = prix
#         self.pro_Visibilite = pro_Visibilite
#         self.couplage = couplage
#         self.type_transaction = type_transaction
#         self.id_type_chauffage = id_type_chauffage
#         self.id_type_cuisine = id_type_cuisine
#         self.balcon = balcon
#         self.eau = eau
#         self.bain = bain
#         self.mensualite = mensualite
#         self.id_publication = id_publication
#         self.id_tt = id_tt
#         self.id_type_bien = id_type_bien
#         self.type_bien = type_bien
#         self.tel_number = tel_number
#         self.object_mail = object_mail
#         self.intro_mail = intro_mail
#         self.ville = ville
#         self.id_quartier = id_quartier
#         self.nom_quartier = nom_quartier
#         self.description = description
#         self.insert_date = insert_date

db.create_all()

if __name__ == '__main__':
    # stag_se_link_detail_ads = StagSeLinkDetailAd.query. \
    #     filter(StagSeLinkDetailAd.filter_id == 1). \
    #     filter(StagSeLinkDetailAd.link_detail.like("%seloger.com%")).all()
    # for i in stag_se_link_detail_ads:
    #     print(i.link_detail)
    pass
