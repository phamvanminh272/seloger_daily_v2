from db_connection.mysql_ssh import db_remote, ssh
from datetime import datetime
db=db_remote
class RealEstate(db.Model):
    __tablename__ = 'real_estate'
    id = db.Column(db.Integer, primary_key=True)
    link_detail = db.Column(db.String(250))
    id_annonce = db.Column(db.String(50))
    code_insee = db.Column(db.String(100))
    title = db.Column(db.String(250))
    description = db.Column(db.Text)
    transaction = db.Column(db.String(50))
    picture = db.Column(db.Text)
    region_name = db.Column(db.String(75))
    department_name = db.Column(db.String(75))
    city = db.Column(db.String(75))
    zipcode = db.Column(db.String(75))
    urgent = db.Column(db.String(50))
    price = db.Column(db.String(100))
    date = db.Column(db.String(75))
    charges_included = db.Column(db.String(250))
    real_estate_type = db.Column(db.String(100))
    floor = db.Column(db.String(75))
    room = db.Column(db.String(50))
    bedroom = db.Column(db.String(50))
    bathroom = db.Column(db.String(50))
    water = db.Column(db.String(50))
    balcon = db.Column(db.String(75))
    furnished = db.Column(db.String(50))
    surface = db.Column(db.String(100))
    ges = db.Column(db.String(100))
    ges_detail = db.Column(db.String(100))
    energy = db.Column(db.String(100))
    energy_detail = db.Column(db.String(100))
    tel_number = db.Column(db.String(100))
    extra_infor = db.Column(db.Text)
    updated = db.Column(db.String(100))
    insert_date = db.Column(db.DateTime)
    web = db.Column(db.String(100))

    def __init__(self, link_detail=None, id_annonce=None, code_insee=None, title=None, description=None,
                 transaction=None, picture=None, region_name=None, department_name=None, city = None,
                 zipcode = None, urgent = None, price = None, date = None, charges_included = None,
                 real_estate_type = None, floor = None, room = None, bedroom = None, bathroom = None, water= None,
                 balcon = None, furnished = None, surface = None, ges = None, ges_detail = None, energy = None,
                 energy_detail = None, tel_number = None, extra_infor = None, updated = None, insert_date = None,
                 web = None):
        self.link_detail = link_detail
        self.id_annonce = id_annonce
        self.code_insee = code_insee
        self.title = title
        self.description = description
        self.transaction = transaction
        self.picture = picture
        self.region_name = region_name
        self.department_name = department_name
        self.city = city
        self.zipcode = zipcode
        self.urgent = urgent
        self.price = price
        self.date = date
        self.charges_included = charges_included
        self.real_estate_type = real_estate_type
        self.floor = floor
        self.room = room
        self.bedroom = bedroom
        self.bathroom = bathroom
        self.water = water
        self.balcon = balcon
        self.furnished = furnished
        self.surface = surface
        self.ges = ges
        self.ges_detail = ges_detail
        self. energy = energy
        self. energy_detail = energy_detail
        self.tel_number = tel_number
        self.extra_infor = extra_infor
        self.updated = updated
        self.insert_date = insert_date
        self.web = web
    def set_atrr_by_dict(self, dict):
        # ls_attr = [self.link_detail, self.id_annonce, self.code_insee, self.title, self.description,
        #          self.transaction, self.picture, self.region_name, self.department_name, self.city,
        #          self.zipcode, self.urgent, self.price, self.date, self.charges_included,
        #          self.real_estate_type, self.floor, self.room, self.bedroom, self.bathroom, self.water,
        #          self.balcon, self.furnished, self.surface, self.ges, self.ges_detail, self.energy,
        #          self.energy_detail, self.tel_number, self.extra_infor, self.updated, self.insert_date,
        #          self.web]
        ls_attr_name = ['link_detail', 'id_annonce', 'code_insee', 'title', 'description',
        'transaction', 'picture', 'region_name', 'department_name', 'city',
        'zipcode', 'urgent', 'price', 'date', 'charges_included',
        'real_estate_type', 'floor', 'room', 'bedroom', 'bathroom', 'water',
        'balcon', 'furnished', 'surface', 'ges', 'ges_detail', 'energy',
        'energy_detail', 'tel_number', 'extra_infor', 'updated', 'insert_date',
        'web']
        extra_infor = ''
        for key, value in dict.items():
            if key not in ls_attr_name and value != None:
                temp = "'"+key+"': "+str(value)+"; "
                extra_infor = extra_infor + temp
        # print(extra_infor[:-1])
        # ls = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self, a))]
        # print(ls)
        # for index in range(len(ls_attr_name)):
        #     if ls_attr_name[index] in dict:
        #         ls_attr[index] = dict[ls_attr_name[index]]
        # self_dict = self.__dict__
        # for key, value in self_dict.items():
        #     print(key, value)
        self.link_detail = dict['link_detail']
        self.id_annonce = dict['id_annonce']
        self.code_insee = dict['code_insee']
        self.title = dict['title']
        self.description = dict['description']
        self.transaction = dict['transaction']
        self.picture = dict['picture']
        self.region_name = dict['region_name']
        self.department_name = dict['department_name']
        self.city = dict['city']
        self.zipcode = dict['zipcode']
        self.urgent = dict['urgent']
        self.price = dict['price']
        self.date = dict['date']
        self.charges_included = dict['charges_included']
        self.real_estate_type = dict['real_estate_type']
        self.floor = dict['floor']
        self.room = dict['room']
        self.bedroom = dict['bedroom']
        self.bathroom = dict['bathroom']
        self.water = dict['water']
        self.balcon = dict['balcon']
        self.furnished = dict['furnished']
        self.surface = dict['surface']
        self.ges = dict['ges']
        self.ges_detail = dict['ges_detail']
        self.energy = dict['energy']
        self.energy_detail = dict['energy_detail']
        self.tel_number = dict['tel_number']
        if extra_infor != '':
            self.extra_infor = extra_infor
        else:
            self.extra_infor = dict['description']
        self.updated = dict['updated']
        self.insert_date = datetime.now()
        self.web = dict['web']
    def compare(self, other):
        other_dict = other.__dict__
        del other_dict['_sa_instance_state']
        del other_dict['id']
        del other_dict['insert_date']
        self_dict = self.__dict__
        del self_dict['_sa_instance_state']
        del self_dict['id']
        del self_dict['insert_date']
        if other_dict == self_dict:
            return True
        return True
print('Run models remote succecss!')

if __name__ == '__main__':
    ssh.stop()








