#!/bin/bash
python3 -m venv venv
echo Create venv done!
echo Start install python packages
source venv/bin/activate
pip install -r requirements.txt
echo Start init database
python init_system.py